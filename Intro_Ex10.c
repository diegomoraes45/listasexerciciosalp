#include <stdio.h>
#include <math.h>


void main()
{


//Declaração de Variáveis

	float codigosaida, codigomeio, codigochegada;



//Entrada de Dados

	printf("Informe o código de saída: \n");
	scanf("%f",&codigosaida);


//Processamento
    //Gerar código na metade da distância
	codigomeio=(codigosaida*3)+32;

	//Gerar código chegada
	codigochegada=sqrt(codigomeio-3);


//Saída de Informações
	
    printf("Código de saída: %.1f \n",codigosaida);
    printf("Código de chegada: %.1f \n",codigochegada);

	
}